from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.template import loader
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .forms import LoginForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages

from .models import Question, Choice

# Create your views here.
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:30]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)
    # template = loader.get_template('polls/index.html')
    # context ={
    #     'latest_question_list': latest_question_list
    # }
    # return HttpResponse(template.render(context, request))
 

def detail(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    # try:
    #     question = Question.objects.get(pk = question_id)
    # except Question.DoesNotExist:
    #     raise Http404('Question does not exist')
    return render(request, 'polls/detail.html', {'question':question})

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    try:
        selected_choice = question.choice_set.get(pk =request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': 'You did not select a choice.'
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
    return HttpResponseRedirect(reverse('polls:results', args = (question.id,)))

def results(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    return render(request, 'polls/results.html', {'question': question})

def sign_in(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('/polls/')
        
        form = LoginForm()
        return render(request, 'polls/login.html', {'form': form})
    
    elif request.method == 'POST':
        form = LoginForm(request.POST)

        if(form.is_valid()):
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username = username, password = password)

            if user:
                login(request, user)
                messages.success(request, f'Hi {username.title()}, welcome back!')
                return redirect('polls:index')
            
        messages.error(request, f'Invalid username or password')
        return render(request,'polls/login.html',{'form': form})
    
def sign_out(request):
    logout(request)
    messages.success(request, f'You have been logged out.')
    return redirect('polls:login')