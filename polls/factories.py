import factory
from factory.django import DjangoModelFactory
from django.utils import timezone

from .models import Question, Choice

class QuestionFactory(DjangoModelFactory):
    class Meta:
        model = Question

    question_text = factory.Faker('sentence')
    pub_date = timezone.now()

question = QuestionFactory()
question.question_text
question.pub_date

class ChoiceFactory(DjangoModelFactory):
    class Meta:
        model = Choice
        
    question = factory.SubFactory(QuestionFactory)
    choice_text = factory.Faker('sentence', nb_words = 5, variable_nb_words = True)
    votes = 8
    
choice = ChoiceFactory()
choice.question
choice.choice_text
choice.votes