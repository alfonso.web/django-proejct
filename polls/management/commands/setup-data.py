import random

from django.db import transaction
from django.core.management.base import BaseCommand

from polls.models import Question, Choice
from polls.factories import (QuestionFactory, ChoiceFactory)

NUM_QUESTIONS = 10
NUM_CHOICES = 4

class Command(BaseCommand):
    help = 'Generates test data'

    @transaction.atomic
    def handle(self, *args, **kwargs):
        self.stdout.write('Deleting old data...')
        model = [Question, Choice]
        for m in model:
            m.objects.all().delete()

        self.stdout.write("Creating new data...")
        # Create all the users
        questions = []
        for _ in range(NUM_QUESTIONS):
            question_factory = QuestionFactory()
            questions.append(question_factory)
            
        for _ in range(NUM_CHOICES):
            question = random.choice(questions)
            ChoiceFactory(question = question)
