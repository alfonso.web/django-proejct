FROM python:3

WORKDIR /usr/src/app

RUN python -m pip install --upgrade pip

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

# RUN django-admin startproject mysite

COPY . ./

# EXPOSE 8000
# CMD ["python", "prueba.py", "runserver", "0.0.0.0:8000"]