# Dockerized Django project
#### Build image python with dockerfile
```
docker build .
```
#### Run image python and create project django
```
docker run -it --volume $(pwd):/code <hash-image> python -m django startproject dockerproject .
```
#### Setup environment variables file(.env)
```
###< docker ###
PROJECT_NAME=docker_python
###> docker ###
```
#### Setup services
```
docker-compose up -d
```
#### Setup migrations
```
docker exec -it docker_django_web bash
```
```
python manage.py migrate
```